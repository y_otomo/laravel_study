<!DOCTYPE html>
<html>
    <head>
        <meta charset='utf-8'>
        <title>user.test</title>
        <style>body {padding: 30px;}</style>
    </head>
    <body>
        <p>
            <a href={{ route('user.list') }}>Home</a>
            <a href={{ route('user.new') }}>会員登録</a>
            <a href={{ route('company.new') }}>会社登録</a>
        </p>
        <h1>user profile</h1>

        <table>
            <tr>
                <th>ID</th><th>名前</th><th>かな</th><th>email</th><th>tel</th><th>性別</th><th>年齢</th><th>会社</th><th>会社tel</th><th>住所</th>
            </tr>
            @foreach($users as $user)
                    <tr>
                        <td>{{ $user['id']}}</td>
                        <td>{{ $user['name'] }}</td>
                        <td>{{ $user['kana'] }}</td>
                        <td>{{ $user['email'] }}</td>
                        <td>{{ $user['tel'] }}</td>
                        @if($user['sex'] == 0)
                            <td>男性</td>
                        @else
                            <td>女性</td>
                        @endif
                        <td>{{ $user['age'] }}</td>
{{--                    @foreach($companies as $company)--}}
{{--                        <td>{{ $company['name'] }}</td>--}}
{{--                        <td>{{ $company['tel'] }}</td>--}}
{{--                        <td>{{ $company['address'] }}</td>--}}
{{--                    @endforeach--}}
                </tr>
            @endforeach
        </table>
    </body>
</html>
