<!DOCTYPE html>
<html>
    <head>
        <meta charset='utf-8'>
        <title>user.test</title>
        <style>
            body {
                padding: 30px;
            }
            div {
                margin-top: 10px;
            }
        </style>
    </head>
    <body>
        <p>
            <a href={{ route('user.list') }}>Home</a>
            <a href={{ route('user.new') }}>会員登録</a>
            <a href={{ route('company.new') }}>会社登録</a>
        </p>
        <h1>入力確認</h1>

        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <div>
            <label >お名前:{{ $user['name'] }}</label>
        </div>
        <div>
            <label>ふりがな:{{ $user['kana'] }}</label>
        </div>
        <div>
            <label>email:{{ $user['email'] }}</label>
        </div>
        <div>
            <label>電話番号:{{ $user['tel'] }}</label>
        </div>
        <div>
            @if($user['sex'] == 0)
                <label>性別:男性</label>
            @else
                <label>性別:女性</label>
            @endif
        </div>
        <div>
            <label>年齢:{{ $user['age'] }}</label>
        </div>

        <form action="{{ route('user.store') }}" method='post' accept-charset='utf-8'>
            @csrf
                <input name='name' type='hidden' value="{{ $user['name'] }}">
                <input name='kana' type='hidden' value="{{ $user['kana'] }}">
                <input name='email' type='hidden' value="{{ $user['email'] }}">
                <input name='tel' type='hidden' value="{{ $user['tel'] }}">
                <input name='sex' type='hidden' value="{{ $user['sex'] }}">
                <input name='age' type='hidden' value="{{ $user['age'] }}">
            <div>
                <input type='submit' value='送信'>
            </div>
        </form>
    </body>
</html>
