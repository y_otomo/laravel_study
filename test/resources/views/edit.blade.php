<!DOCTYPE html>
<html>
    <head>
        <meta charset='utf-8'>
        <title>user.test</title>
        <style>
            body {
                padding: 30px;
            }
            div {
                margin-top: 10px;
            }
        </style>
    </head>
    <body>
        <p>
            <a href={{ route('user.list') }}>Home</a>
            <a href={{ route('user.new') }}>会員登録</a>
        </p>
        <h1>会員編集</h1>

        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <form action="{{ route('user.reconfirm') }}" method='post' accept-charset='utf-8'>
            @csrf
            <input name='id' type='hidden' value='{{ $user['id'] }}'>
            <div>
                <label>お名前:</label>
                <input name='name' type='text' value='{{ old( 'name', $user['name']) }}' >
            </div>
            <div>
                <label>ふりがな:</label>
                <input name='kana' type='text' value='{{old('kana', $user['kana'])  }}'>
            </div>
            <div>
                <label>email:</label>
                <input name='email' type='email' value='{{ old('email', $user['email']) }}'>
            </div>
            <div>
                <label>電話番号:</label>
                <input name='tel' type='text' value='{{ old('tel', $user['tel']) }}'>
            </div>
            <div>
                <label>性別</label>
                <input name='sex' type='radio' value="0" @if(old('sex', $user['sex']) == 0 )checked @endif>男性
                <input name='sex' type='radio' value="1" @if(old('sex', $user['sex']) == 1 )checked @endif >女性

            </div>
            <div>
                <label>年齢:</label>
                <select name='age'>
                    <option value="18" @if(old('age', $user['age']) == 18 )selected @endif>18</option>
                    <option value="19" @if(old('age', $user['age']) == 19 )selected @endif>19</option>
                    <option value="20" @if(old('age', $user['age']) == 20 )selected @endif>20</option>
                    <option value="21" @if(old('age', $user['age']) == 21 )selected @endif>21</option>
                </select>
            </div>
            <div>
                <input type='submit' value='確認'>
            </div>
        </form>
    </body>
</html>
