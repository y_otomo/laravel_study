<!DOCTYPE html>
<html>
    <head>
        <meta charset='utf-8'>
        <title>user.test</title>
        <style>
            body {
                padding: 30px;
            }
            div {
                margin-top: 10px;
            }
        </style>
    </head>

    <body>
        <p>
            <a href={{ route('user.list') }}>Home</a>
            <a href={{ route('user.new') }}>会員登録</a>
            <a href={{ route('company.new') }}>会社登録</a>
        </p>
        <h1>会社登録</h1>

        <form action="{{ route('company.confirm') }}" method='post' accept-charset='utf-8'>
            @csrf
            <div>
                <label >会社名:</label>
                <input name='name' type='text' >
            </div>
            <div>
                <label >電話番号:</label>
                <input name='tel' type='text' >
            </div>
            <div>
                <label >住所:</label>
                <input name='address' type='text' >
            </div>
            <div>
                <input type='submit' value='確認'>
            </div>
        </form>
    </body>
</html>
