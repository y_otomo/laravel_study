<!DOCTYPE html>
<html>
    <head>
        <meta charset='utf-8'>
        <title>user.test</title>
        <style>
            body {
                padding: 30px;
            }
            div {
                margin-top: 10px;
            }
        </style>
    </head>
    <body>
        <p>
            <a href={{ route('user.list') }}>Home</a>
            <a href={{ route('user.new') }}>会員登録</a>
            <a href={{ route('company.new') }}>会社登録</a>
        </p>
        <h1>新規登録</h1>

        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <form action="{{ route('user.confirm') }}" method='post' accept-charset='utf-8'>
            @csrf
            <div>
                <label >お名前:</label>
                <input name='name' type='text' value="{{ old('name') }}" >
            </div>
            <div>
                <label>ふりがな:</label>
                <input name='kana' type='text' value="{{ old('kana') }}">
            </div>
            <div>
                <label>email:</label>
                <input name='email' type='text' value="{{ old('email') }}">
            </div>
            <div>
                <label>電話番号:</label>
                <input name='tel' type='text' value="{{ old('tel') }}">
            </div>
            <div>
                <label>性別:</label>
                <input name='sex' type='radio' value="0" @if(old('sex') == "0")checked ='checked' @endif>男性
                <input name='sex' type='radio' value="1" @if(old('sex') == "1")checked ='checked' @endif>女性
            </div>
            <div>
                <label>年齢:</label>
                <select name='age'>
                    <option value="18">18</option>
                    <option value="19">19</option>
                    <option value="20">20</option>
                    <option value="21">21</option>
                </select>
            </div>
            <div>
                <label>会社:</label>
{{--                @foreach($companies as $company)--}}
{{--                    <option>{{ $company[''] }}</option>--}}
{{--                @endforeach--}}
            </div>
            <div>
                <input type='submit' value='確認'>
            </div>
        </form>
        <a href={{ route('user.list') }}>一覧画面</a>
    </body>
</html>
