<!DOCTYPE html>
<html>
    <head>
        <meta charset='utf-8'>
        <title>user.test</title>
        <style>
            body {
                padding: 30px;
            }
            div {
                margin-top: 10px;
            }
        </style>
    </head>

    <body>
        <p>
            <a href={{ route('user.list') }}>Home</a>
            <a href={{ route('user.new') }}>会員登録</a>
            <a href={{ route('company.new') }}>会社登録</a>
        </p>
        <h1>入力確認</h1>
        <div>
            <label >会社名:{{ $company['name'] }}</label>
        </div>
        <div>
            <label>電話番号:{{ $company['tel'] }}</label>
        </div>
        <div>
            <label>住所:{{ $company['address'] }}</label>
        </div>

        <form action="{{ route('company.store') }}" method='post' accept-charset='utf-8'>
            @csrf
            <input name='name' type='hidden' value="{{ $company['name'] }}">
            <input name='tel' type='hidden' value="{{ $company['tel'] }}">
            <input name='address' type='hidden' value="{{ $company['address'] }}">
            <div>
                <input type='submit' value='送信'>
            </div>
        </form>
    </body>
</html>
