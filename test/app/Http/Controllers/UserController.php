<?php

namespace App\Http\Controllers;

use App\Company;
use App\Http\Models\User;
use App\Http\servicies\CompanyService;
use App\Http\servicies\UserService;
use Illuminate\Http\Request;

class UserController extends Controller
{
    private $userService;
    private $companyService;
    public function __construct(UserService $userService, CompanyService $companyService)
    {
        $this->userService=$userService;
        $this->companyService=$companyService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = $this->userService->getAll();
        $companies = $this->companyService->getAll();
        return view('index', ['users' => $users, 'companies'=>$companies]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $companies = Company::all()->pluck('name', 'id');
        return view('new', ['companies' => $companies]);
    }

    public function confirm(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'kana' => 'required',
            'email' => 'required|email',
            'tel' => 'required',
            'sex' => 'required',
        ]
//            , [
//            'name.required' => '名前を入力してください',
//            'kana.required' => 'ふりがなを入力してください',
//            'email.required' => 'メールアドレスで入力してください',
//            'tel.required' => '電話番号を入力してください',
//            'sex.required' => '性別を選択してください',
//        ]
        );

        $user = $request->all();
        return view('confirm',['user' => $user]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->userService->store($request);
        return redirect()->route('user.list');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Http\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        $users = $this->userService->getAll();
        dd($users->companies);
//        $companies = $this->companyService->getAll();
        return view('show', ['users' => $users/*, 'companies'=>$companies*/]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Http\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request,$id)
    {
        $user = $this->userService->edit($id);
        return view('edit', ['user' => $user]);
    }

    public function reconfirm(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'kana' => 'required',
            'email' => 'required|email',
            'tel' => 'required',
            'sex' => 'required',
        ]);

        return view('reconfirm', ['user' => $request]);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Http\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $this->userService->update($request);
        return redirect()->route('user.list');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Http\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        $user = $this->userService->delete($id);
        return view('delete',['user'=> $user]);
    }

    public function remove(Request $request)
    {
        $this->userService->remove($request);
        return redirect()->route('user.list');
    }

}
