<?php


namespace App\Http\servicies;


use App\Http\repositories\UserRepository;
use http\Env\Request;

class UserService
{

    private $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository=$userRepository;
    }

    public function getAll()
    {
        $users= $this->userRepository->getAll();
       return $users;
    }

    public function store($request)
    {
        $this->userRepository->store($request);
    }

    public function edit($id)
    {
        $user = $this->userRepository->edit($id);
        return $user;
    }

    public function update($request)
    {
        $this->userRepository->update($request);
    }

    public function delete($id)
    {
        $user = $this->userRepository->delete($id);
        return $user;
    }

    public function remove($request)
    {
        $this->userRepository->remove($request);
    }

}
