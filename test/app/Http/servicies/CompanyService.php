<?php


namespace App\Http\servicies;

use App\Http\repositories\CompanyRepository;

class CompanyService
{
    private $companyRepository;

    public function __construct(companyRepository $companyRepository)
    {
        $this->companyRepository=$companyRepository;
    }

    public function getAll()
    {
        $companies = $this->companyRepository->getAll();
        return $companies;
    }

    public function store($request)
    {
        $this->companyRepository->store($request);
    }
}
