<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    public function companies()
    {
        return $this->belongsToMany('App\Company');
    }
}
