<?php


namespace App\Http\repositories;


use App\Company;


class CompanyRepository
{
    public function getAll()
    {
        $companies = Company::all();
        return $companies;
    }

    public function store()
    {
        $company = new Company;
        $company->name = request('name');
        $company->tel = request('tel');
        $company->address = request('address');

        $company->save();
    }
}
