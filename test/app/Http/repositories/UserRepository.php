<?php


namespace App\Http\repositories;


use App\Http\Models\User;
use http\Env\Request;
use Symfony\Component\Console\Input\Input;

class UserRepository
{
    public function getAll()
    {
        $users = User::all();
        return $users;
    }

    public function store()
    {
        $user = new User;
        $user->name = request('name');
        $user->kana = request('kana');
        $user->email = request('email');
        $user->tel = request('tel');
        $user->sex = request('sex');
        $user->age = request('age');

        $user->save();
    }

    public function edit($id)
    {
        $user = User::find($id);
        return $user;
    }

    public function update()
    {
//        dd(request());
        $user = User::find(request('id'));
        $user->name = request('name');
        $user->kana = request('kana');
        $user->email = request('email');
        $user->tel = request('tel');
        $user->sex = request('sex');
        $user->age = request('age');

        $user->save();

    }

    public function delete($id)
    {
        $user = User::find($id);
        return $user;
    }

    public function remove($request)
    {
        $user = User::find($request['id']);
        $user->delete();
    }

}
