<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/test.user', 'UserController@index')->name('user.list');
Route::get('/test.user/new', 'UserController@create')->name('user.new');
Route::post('/test.user/new/confirm', 'UserController@confirm')->name('user.confirm');
Route::post('/test.user/new/store', 'UserController@store')->name('user.store');
Route::get('/test.user/edit/{id}', 'UserController@edit')->name('user.edit');
Route::post('/test.user/reconfirm', 'UserController@reconfirm')->name('user.reconfirm');
Route::post('/test.user/update', 'UserController@update')->name('user.update');
Route::get('/test.user/delete/{id}','UserController@delete')->name('user.delete');
Route::post('/test.user/delete', 'UserController@remove')->name('user.remove');

Route::get('/test.user/new.company', 'CompanyController@create')->name('company.new');
Route::post('test.user/new.company/confirm.company','CompanyController@confirm')->name('company.confirm');
Route::post('test.user/new.company/store.company', 'CompanyController@store')->name('company.store');

Route::get('/test.user/show', 'UserController@show')->name('user.show');

Route::get('/', function () {
    return redirect('/test.user');
});
